# MagicWord Japonais

Notre projet a pour objectif l'introduction de la langue japonaise dans le lexique du MagicWord.
Le code que nous avons mis c'est pour restructurer le lexique du Dictionnaire Cesselin japonais-français utilisé pour Jibiki.

Nous avons supprimé des colonnes pour avoir seulement id, le mot en hiragana, 
le mot en kanji et la catégorie grammaticale afin que nous ayons la même structure que pour le lexique français.