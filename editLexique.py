import re
import sys
from turtledemo.chaos import g


class TextFile:
    """ exemple de classe
    """

    def __init__(self, name, encoding="utf8"):

        self._name = name
        self._encoding = encoding
        self._content=""

    def read(self):
        try:
            f = open(self._name, encoding=self._encoding, mode='r')
            self._content = f.read()

            f.close()
        except OSError as err:
            print("OS error: {0}".format(err))

    def saveAs(self, newFileName):
        try:
            f = open(newFileName, encoding=self._encoding, mode='w')
            self._content = re.sub(r"(?s)<article.*?hiragana>", "", self._content)
           
            self._content = re.sub(r"(?s)<exemples>.*?</exemples>", "eol", self._content)
            self._content = re.sub(r"(?s)</gram>.*?</article>", "", self._content)
            self._content = re.sub("</vedette-hiragana><vedette-jpn>","eol", self._content)
            self._content = re.sub(r"(?s)</vedette-jpn></vedett.*?ine/><gram>", "eol", self._content)
            self._content = re.sub("eol","\t",self._content)




            self._content = re.sub(r"(?s)<vedette-romaji .*?</vedette-romaji>", "", self._content)

            f.write(self._content)
            f.close()
        except OSError as err:
            print("OS error: {0}".format(err))
tf=TextFile("lexique.xml")
tf.read()
tf.saveAs("newLexic1.tsv")