import re
import sys
from turtledemo.chaos import g


class TextFile:
    """ exemple de classe
    """

    def __init__(self, name, encoding="utf-8"):

        self._name = name
        self._encoding = encoding
        self._content=""

    def read(self):
        try:
            f = open(self._name, encoding=self._encoding, mode='r')
            self._content = f.read()

            f.close()
        except OSError as err:
            print("OS error: {0}".format(err))

    def saveAs(self, newFileName):
        try:
            f = open(newFileName, encoding=self._encoding, mode='w')
            self._content = re.sub("ょ", "よ", self._content)
            self._content = re.sub("ゃ", "や", self._content)
            self._content = re.sub("ゅ", "ゆ", self._content)
            self._content = re.sub("っ", "つ", self._content)

            f.write(self._content)
            f.close()
        except OSError as err:
            print("OS error: {0}".format(err))
tf=TextFile("newLexic1.tsv")
tf.read()

tf.saveAs("normalisedLexicon.tsv")