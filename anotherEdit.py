# import re

#  -*- coding: utf-8 -*-


class TextFile:
    """ exemple de classe
    """

    def __init__(self, name, encoding="utf-8"):

        self._name = name
        self._encoding = encoding
        self._content=""


    def read(self):
        try:
            f = open(self._name, encoding=self._encoding, mode='r')
            self._content = f.read()

            f.close()
        except OSError as err:
            print("OS error: {0}".format(err))
 
    def saveAs2(self, newFileName):
        try:
            f2 = open(newFileName, encoding="utf-8", mode='w')
            lines=self._content.splitlines()
            for line in lines:
                t = line.split("\t")

                if(len(t)==3):
                    x = str(t[1])
                    for i in range(0, len(x)):
                        f2.write(str(t[0]))
                        f2.write("\t")
                        f2.write(x[i])
                        f2.write("\t")
                        f2.write(str(t[2]))
                        f2.write("\n")



            f2.close()
        except OSError as err:
            print("OS error: {0}".format(err))

tf=TextFile("newLexic1.tsv")
tf.read()

tf.saveAs2("editedLexion1.tsv")

# import csv

